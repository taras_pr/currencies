package ua.com.vtm.currencies.presentation

import ua.com.vtm.core.common.MyConstants
import ua.com.vtm.features.currencies.data.local.rates.RateDto
import org.junit.Assert.*
import org.junit.Test
import ua.com.vtm.features.currencies.presentation.RatePresentation
import ua.com.vtm.features.currencies.presentation.RatesMapper

/**
 * @author vtm on 2021-04-18.
 */
class RatesMapperTest {

    private val ratesMapper = RatesMapper()

    @Test
    fun `when rates mapper should return correct value`() {

        val list = listOf(RateDto(symbol = MyConstants.BRL))
        val listConverted = listOf(RatePresentation(symbol = MyConstants.BRL))

        val actual = ratesMapper.mapperListToRatePresentation(list)
        assertEquals(listConverted.first().symbol, actual.first()?.symbol)
    }
}