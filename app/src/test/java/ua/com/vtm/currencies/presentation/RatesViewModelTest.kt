package ua.com.vtm.currencies.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import ua.com.vtm.features.currencies.data.local.rates.RateDto
import ua.com.vtm.features.currencies.domain.usecases.ChangeRateUseCase
import ua.com.vtm.features.currencies.domain.usecases.FetchRatesUseCase
import ua.com.vtm.features.currencies.domain.usecases.RefreshRatesUseCase
import ua.com.vtm.utils.MainCoroutineRule
import ua.com.vtm.utils.getOrAwaitValue
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import ua.com.vtm.core.common.BaseCoroutineDispatcher
import ua.com.vtm.core.common.MyConstants
import ua.com.vtm.features.currencies.presentation.*

/**
 * @author vtm on 2021-04-18.
 */

@ExperimentalCoroutinesApi
class RatesViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutineRule = MainCoroutineRule()

    private val dispatcher = mockk<BaseCoroutineDispatcher>()
    private val ratesMapper = mockk<RatesMapper>(relaxUnitFun = true)
    private val fetchRatesUseCase = mockk<FetchRatesUseCase>(relaxUnitFun = true)
    private val changeRateUseCase = mockk<ChangeRateUseCase>(relaxUnitFun = true)
    private val refreshRatesUseCase = mockk<RefreshRatesUseCase>(relaxUnitFun = true)

    private lateinit var viewModel: RatesViewModel

    @Before
    fun setup() {

        viewModel = RatesViewModel(dispatcher, ratesMapper, fetchRatesUseCase, changeRateUseCase, refreshRatesUseCase)
        mockDispatcher()
        viewModel.events.observeForever {  }
    }

    private fun mockDispatcher() {

        every { dispatcher.IO() } returns coroutineRule.testDispatcher
        every { dispatcher.Main() } returns Dispatchers.Main
    }

    @Test
    fun `when invoke action InitPageAction should return RatesLoadingState`() = coroutineRule.runBlockingTest {

        viewModel.invokeAction(RatesViewContract.Action.InitPageAction)

        val expected = RatesViewContract.ViewState.State.RatesLoadingState
        assertEquals(expected, viewModel.states.getOrAwaitValue())
    }

    @Test
    fun `when invoke action InitPageAction should call for fetch rates use case`() = coroutineRule.runBlockingTest {

        viewModel.invokeAction(RatesViewContract.Action.InitPageAction)
        coVerify(exactly = 1) { fetchRatesUseCase.invoke(any())  }
    }

    @Test
    fun `when invoke action ChangeRateAction should call for change rate use case`() = coroutineRule.runBlockingTest {

        val ratePresentation = RatePresentation(symbol = MyConstants.BRL)
        viewModel.invokeAction(RatesViewContract.Action.ChangeRateAction(ratePresentation))

        coVerify(exactly = 1) { changeRateUseCase.invoke(any()) }
    }

    @Test
    fun `when request fetch rates should return RatesSuccessState`() = coroutineRule.runBlockingTest {

        val list = listOf(RateDto())
        val listConverted = listOf(RatePresentation(symbol = MyConstants.BRL))
        viewModel.requestFetchRates.value = list

        every { ratesMapper.mapperListToRatePresentation(any()) } returns listConverted

        val expected = RatesViewContract.ViewState.State.RatesSuccessState(RatesPresentationResponse(listConverted))
        assertEquals(expected, viewModel.states.getOrAwaitValue())
    }
}