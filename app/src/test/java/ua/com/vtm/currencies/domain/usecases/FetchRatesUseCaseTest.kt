package ua.com.vtm.currencies.domain.usecases

import ua.com.vtm.core.common.Either
import ua.com.vtm.core.common.UseCase
import ua.com.vtm.features.currencies.data.local.rates.RateDto
import ua.com.vtm.features.currencies.domain.RatesRepository
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import ua.com.vtm.features.currencies.domain.usecases.FetchRatesUseCase

/**
 * @author vtm on 2021-04-18.
 */
class FetchRatesUseCaseTest {

    private val repository = mockk<RatesRepository>()
    private lateinit var useCase: FetchRatesUseCase

    @Before
    fun setup() {
        useCase = FetchRatesUseCase(repository)
    }

    @Test
    fun `when run use case should call repository fetch rates`() = runBlocking {

        val list = listOf(RateDto())
        val flowSource = flow { emit(list) }
        every { repository.fetchRates() } returns Either.right(flowSource)

        useCase.run(UseCase.None())
        coVerify(exactly = 1) { repository.fetchRates() }
    }
}