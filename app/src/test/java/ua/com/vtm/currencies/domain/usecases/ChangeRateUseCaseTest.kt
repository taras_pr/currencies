package ua.com.vtm.currencies.domain.usecases

import ua.com.vtm.core.common.Either
import ua.com.vtm.core.common.MyConstants
import ua.com.vtm.features.currencies.domain.RatesRepository
import ua.com.vtm.features.currencies.presentation.RatePresentation
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import ua.com.vtm.features.currencies.domain.usecases.ChangeRateUseCase

/**
 * @author vtm on 2021-04-18.
 */
class ChangeRateUseCaseTest {

    private val repository = mockk<RatesRepository>()
    private lateinit var useCase: ChangeRateUseCase

    @Before
    fun setup() {
        useCase = ChangeRateUseCase(repository)
    }

    @Test
    fun `when run use case should call repository change rate`() = runBlocking {

        val ratePresentation = RatePresentation(symbol = MyConstants.BRL)
        coEvery { repository.changeRate(ratePresentation) } returns Either.right(Unit)

        useCase.run(ratePresentation)
        coVerify(exactly = 1) { repository.changeRate(ratePresentation) }
    }
}