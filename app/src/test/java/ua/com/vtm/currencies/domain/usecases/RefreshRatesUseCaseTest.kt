package ua.com.vtm.currencies.domain.usecases

import ua.com.vtm.core.common.Either
import ua.com.vtm.core.common.UseCase
import ua.com.vtm.features.currencies.domain.RatesRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import ua.com.vtm.features.currencies.domain.usecases.RefreshRatesUseCase


/**
 * @author vtm on 2021-04-18.
 */
class RefreshRatesUseCaseTest {

    private val repository = mockk<RatesRepository>()
    private lateinit var useCase: RefreshRatesUseCase

    @Before
    fun setup() {
        useCase = RefreshRatesUseCase(repository)
    }

    @Test
    fun `when run use case should call repository refresh rates`() = runBlocking {

        coEvery { repository.refreshRates() } returns Either.right(Unit)

        useCase.run(UseCase.None())
        coVerify(exactly = 1) { repository.refreshRates() }
    }
}