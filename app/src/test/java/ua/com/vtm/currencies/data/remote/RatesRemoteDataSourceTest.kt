package ua.com.vtm.currencies.data.remote

import ua.com.vtm.core.common.MyConstants
import ua.com.vtm.core.datasource.remote.NetworkHandler
import ua.com.vtm.core.datasource.remote.RemoteApi
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import ua.com.vtm.features.currencies.data.remote.RatesRemoteDataSource
import ua.com.vtm.features.currencies.data.remote.RatesRemoteDataSourceImpl
import ua.com.vtm.features.currencies.data.remote.RatesRemoteResponse

/**
 * @author vtm on 2021-04-17.
 */
class RatesRemoteDataSourceTest {

    private val remoteApi = mockk<RemoteApi>()
    private val networkHandler = mockk<NetworkHandler>()

    private lateinit var remoteDataSource: RatesRemoteDataSource

    @Before
    fun setup() {

        remoteDataSource = RatesRemoteDataSourceImpl(remoteApi, networkHandler)
        every { networkHandler.isConnected } returns true
    }

    @Test
    fun `when fetch rates should call remoteApi fetch rates and return a RatesRemoteResponse`() = runBlocking {

        val symbol = MyConstants.EUR
        val expectedRemoteResponse = RatesRemoteResponse.empty()
        coEvery { remoteApi.fetchRates(symbol) } returns expectedRemoteResponse

        val remoteResponse = remoteDataSource.fetchRates().rightOrNull()
        coVerify(exactly = 1) {remoteApi.fetchRates(symbol)}
        assertEquals(expectedRemoteResponse, remoteResponse)
    }
}