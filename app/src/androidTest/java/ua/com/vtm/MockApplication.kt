package ua.com.vtm

import android.app.Application
import ua.com.vtm.core.common.BaseAppComponent
import ua.com.vtm.di.DaggerAppTestComponent

/**
 * @author vtm on 2021-04-18.
 */
class MockApplication: Application(), BaseApplication {

    override val appComponent: BaseAppComponent by lazy {
        DaggerAppTestComponent
            .builder()
            .build()
    }
}