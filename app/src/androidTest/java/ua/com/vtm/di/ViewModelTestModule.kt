package ua.com.vtm.di

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import io.mockk.mockk
import javax.inject.Singleton

/**
 * @author vtm on 2021-04-19.
 */
@Module
class ViewModelTestModule {

    @Provides
    @Singleton
    fun provideViewModelFactory(): ViewModelProvider.Factory = mockk()
}
