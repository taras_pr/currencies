package ua.com.vtm.di

import androidx.lifecycle.ViewModelProvider
import ua.com.vtm.core.common.BaseAppComponent
import ua.com.vtm.core.di.component.ActivityComponent
import dagger.Component
import javax.inject.Singleton

/**
 * @author vtm on 2021-04-19.
 */
@Singleton
@Component(modules = [
    ViewModelTestModule::class
])
interface AppTestComponent: BaseAppComponent {

    override fun activityComponent(): ActivityComponent.Builder
    override val viewModelFactory: ViewModelProvider.Factory
}