package ua.com.vtm.core.common

import androidx.lifecycle.ViewModel

/**
 * @author vtm on 2021-04-18.
 */

abstract class BaseViewModel() : ViewModel() {

    protected val failure by lazy { SingleLiveEvent<Throwable>() }

    open fun handleFailure(failure: Throwable) {
        this.failure.postValue(failure)
    }
}