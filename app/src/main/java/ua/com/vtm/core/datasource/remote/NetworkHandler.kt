package ua.com.vtm.core.datasource.remote

import android.content.Context
import ua.com.vtm.core.extension.networkInfo
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author vtm on 2021-04-18.
 */

@Singleton
class NetworkHandler @Inject constructor(private val context: Context) {
    val isConnected get() = context.networkInfo?.isConnectedOrConnecting
}
