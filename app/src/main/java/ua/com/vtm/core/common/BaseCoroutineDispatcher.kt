package ua.com.vtm.core.common

import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * @author vtm on 2021-04-18.
 */
class BaseCoroutineDispatcher @Inject constructor() {
    fun Main() = Dispatchers.Main
    fun IO() = Dispatchers.IO
}