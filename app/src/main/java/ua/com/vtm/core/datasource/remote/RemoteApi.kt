package ua.com.vtm.core.datasource.remote

import ua.com.vtm.features.currencies.data.remote.RatesRemoteResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author vtm on 2021-04-18.
 */
interface RemoteApi {

    @GET("latest")
    suspend fun fetchRates(
        @Query("base") baseCurrency: String
    ): RatesRemoteResponse
}