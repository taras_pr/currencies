package ua.com.vtm.core.di.module

import ua.com.vtm.core.datasource.local.AppDatabase
import ua.com.vtm.core.datasource.remote.NetworkHandler
import ua.com.vtm.core.datasource.remote.RemoteApi
import ua.com.vtm.features.currencies.data.local.basecurrency.BaseCurrencyLocalDataSource
import ua.com.vtm.features.currencies.data.local.basecurrency.BaseCurrencyLocalDataSourceImpl
import ua.com.vtm.features.currencies.data.local.rates.RatesLocalDataSource
import ua.com.vtm.features.currencies.data.local.rates.RatesLocalDataSourceImpl
import ua.com.vtm.features.currencies.data.remote.RatesRemoteDataSource
import ua.com.vtm.features.currencies.data.remote.RatesRemoteDataSourceImpl
import ua.com.vtm.features.currencies.domain.DataProducerHandler
import ua.com.vtm.features.currencies.domain.RatesRepository
import ua.com.vtm.features.currencies.domain.RatesRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author vtm on 2021-04-18.
 */

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideRatesLocalDataSource(appDatabase: AppDatabase): RatesLocalDataSource {
        return RatesLocalDataSourceImpl(appDatabase)
    }

    @Singleton
    @Provides
    fun baseCurrencyLocalDataSource(appDatabase: AppDatabase): BaseCurrencyLocalDataSource {
        return BaseCurrencyLocalDataSourceImpl(appDatabase)
    }

    @Singleton
    @Provides
    fun provideRatesRemoteDataSource(
        remoteApi: RemoteApi,
        networkHandler: NetworkHandler
    ): RatesRemoteDataSource {
        return RatesRemoteDataSourceImpl(remoteApi, networkHandler)
    }

    @Singleton
    @Provides
    fun provideDataProducerHandler(): DataProducerHandler {
        return DataProducerHandler()
    }

    @Singleton
    @Provides
    fun provideRatesRepository(
        ratesLocalDataSource: RatesLocalDataSource,
        baseCurrencyLocalDataSource: BaseCurrencyLocalDataSource,
        ratesRemoteDataSource: RatesRemoteDataSource,
        dataProducerHandler: DataProducerHandler
    ): RatesRepository {
        return RatesRepositoryImpl(
            ratesLocalDataSource, baseCurrencyLocalDataSource, ratesRemoteDataSource, dataProducerHandler
        )
    }
}