package ua.com.vtm.core.datasource.remote

/**
 * @author vtm on 2021-04-18.
 */

sealed class RemoteFailure(errorMsg: String) : Throwable(errorMsg) {

    class NetworkConnection : RemoteFailure("Network Connection Error")
    class ServerError : RemoteFailure("Server Error")

    class ErrorLoadingData : RemoteFailure("Error Loading Data")
}