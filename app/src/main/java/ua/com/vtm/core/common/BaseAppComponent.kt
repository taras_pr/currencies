package ua.com.vtm.core.common

import androidx.lifecycle.ViewModelProvider
import ua.com.vtm.core.di.component.ActivityComponent

/**
 * @author vtm on 2021-04-18.
 */
interface BaseAppComponent {

    fun activityComponent(): ActivityComponent.Builder
    val viewModelFactory: ViewModelProvider.Factory
}