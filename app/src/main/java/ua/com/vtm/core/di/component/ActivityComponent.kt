package ua.com.vtm.core.di.component

import ua.com.vtm.features.currencies.presentation.RatesFragment
import dagger.Subcomponent

/**
 * @author vtm on 2021-04-18.
 */

@Subcomponent
interface ActivityComponent {

    fun inject(ratesFragment: RatesFragment)

    @Subcomponent.Builder
    interface Builder {

        fun build(): ActivityComponent
    }
}