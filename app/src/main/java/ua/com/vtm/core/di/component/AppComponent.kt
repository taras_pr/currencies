package ua.com.vtm.core.di.component

import androidx.lifecycle.ViewModelProvider
import ua.com.vtm.core.common.BaseAppComponent
import ua.com.vtm.core.di.module.ApplicationModule
import ua.com.vtm.core.di.module.DatabaseModule
import ua.com.vtm.core.di.module.RemoteModule
import ua.com.vtm.core.di.module.RepositoryModule
import ua.com.vtm.core.di.module.viewmodel.ViewModelModule

import dagger.Component
import javax.inject.Singleton

/**
 * @author vtm on 2021-04-18.
 */
@Singleton
@Component(modules = [
    ApplicationModule::class,
    DatabaseModule::class,
    RemoteModule::class,
    RepositoryModule::class,
    ViewModelModule::class
])
interface AppComponent: BaseAppComponent {

    override fun activityComponent(): ActivityComponent.Builder
    override val viewModelFactory: ViewModelProvider.Factory
}

