package ua.com.vtm.core.common

import ua.com.vtm.core.datasource.remote.NetworkHandler
import ua.com.vtm.core.datasource.remote.RemoteFailure
import timber.log.Timber

/**
 * @author vtm on 2021-04-18.
 */

abstract class BaseRequester(
    private val networkHandler: NetworkHandler
) {

    suspend fun <T, R> request(apiCall: suspend() -> T, transform: (T) -> R, default: T): EitherResult<R> {
        return when (networkHandler.isConnected) {
            true -> requestHttp(apiCall, transform, default)
            false, null -> Either.left(RemoteFailure.NetworkConnection())
        }
    }

    private suspend fun <T, R> requestHttp(apiCall: suspend() -> T, transform: (T) -> R, default: T): EitherResult<R> {
        return try {
            val response = apiCall.invoke()
            Timber.e("Response: $response")
            Either.right(transform((response ?: default)))
        } catch (exception: Throwable) {
            Timber.e("Error: $exception")
            Either.left(RemoteFailure.ServerError())
        }
    }
}