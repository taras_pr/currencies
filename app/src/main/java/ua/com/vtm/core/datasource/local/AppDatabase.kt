package ua.com.vtm.core.datasource.local

import androidx.room.Database
import androidx.room.RoomDatabase
import ua.com.vtm.features.currencies.data.local.basecurrency.BaseCurrencyDao
import ua.com.vtm.features.currencies.data.local.basecurrency.BaseCurrencyDto
import ua.com.vtm.features.currencies.data.local.rates.RateDto
import ua.com.vtm.features.currencies.data.local.rates.RatesDao

/**
 * @author vtm on 2021-04-17.
 */
@Database(entities = [
    RateDto::class,
    BaseCurrencyDto::class
], version = 1)
abstract class AppDatabase: RoomDatabase() {

    abstract fun ratesDao(): RatesDao
    abstract fun baseCurrencyDao(): BaseCurrencyDao
}