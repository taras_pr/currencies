package ua.com.vtm

import ua.com.vtm.core.common.BaseAppComponent


/**
 * @author vtm on 2021-04-18.
 */
interface BaseApplication {

    val appComponent: BaseAppComponent
}