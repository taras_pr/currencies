package ua.com.vtm

import android.app.Application
import ua.com.vtm.core.common.BaseAppComponent
import ua.com.vtm.core.di.component.DaggerAppComponent
import ua.com.vtm.core.di.module.ApplicationModule
import timber.log.Timber

/**
 * @author vtm on 2021-04-19.
 */
class MyApplication: Application(), BaseApplication {

    override val appComponent: BaseAppComponent by lazy {
        DaggerAppComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }
    }
}

