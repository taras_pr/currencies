package ua.com.vtm.features.currencies.presentation

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import ua.com.vtm.core.common.BaseCoroutineDispatcher
import ua.com.vtm.core.common.BaseViewModel
import ua.com.vtm.core.common.SingleLiveEvent
import ua.com.vtm.core.common.UseCase
import ua.com.vtm.features.currencies.data.local.rates.RateDto
import ua.com.vtm.features.currencies.domain.usecases.ChangeRateUseCase
import ua.com.vtm.features.currencies.domain.usecases.FetchRatesUseCase
import ua.com.vtm.features.currencies.domain.usecases.RefreshRatesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author vtm on 2021-04-18.
 */
class RatesViewModel @Inject constructor(
    private val dispatcher: BaseCoroutineDispatcher,
    private val ratesMapper: RatesMapper,
    private val fetchRatesUseCase: FetchRatesUseCase,
    private val changeRateUseCase: ChangeRateUseCase,
    private val refreshRatesUseCase: RefreshRatesUseCase
): BaseViewModel(), RatesViewContract.ViewModel {

    private val action = SingleLiveEvent<RatesViewContract.Action>()

    @VisibleForTesting
    val requestFetchRates = MutableLiveData<List<RateDto?>>()

    private val _states = MediatorLiveData<RatesViewContract.ViewState.State>().apply {

        addSource(requestFetchRates) { list ->
            ratesMapper.mapperListToRatePresentation(list).also { listConverted ->
                if (listConverted.isNotEmpty()) {
                    postValue(
                        RatesViewContract.ViewState.State.RatesSuccessState(
                            RatesPresentationResponse(listConverted)
                        )
                    )
                }
            }
        }
    }

    private val _events = SingleLiveEvent<RatesViewContract.ViewState.Event>().apply {

        addSource(action) {
            handleAction(it)
        }
    }

    override val states: LiveData<RatesViewContract.ViewState.State> = _states
    override val events: LiveData<RatesViewContract.ViewState.Event> = _events

    override fun invokeAction(actionToInvoke: RatesViewContract.Action) {
        action.postValue(actionToInvoke)
    }

    private fun handleAction(action: RatesViewContract.Action) {

        viewModelScope.launch(dispatcher.IO()) {
            when (action) {
                is RatesViewContract.Action.InitPageAction -> {
                    fetchRates()
                }
                RatesViewContract.Action.RefreshRatesAction -> {
                    refreshRatesUseCase.invoke(UseCase.None()).also {
                        it.either(::handleFailure) {}
                    }
                }
                is RatesViewContract.Action.ChangeRateAction -> {
                    changeRateUseCase.invoke(action.rate).also {
                        it.either(::handleFailure) {}
                    }
                }
            }
        }
    }

    private suspend fun fetchRates() {

        _states.postValue(RatesViewContract.ViewState.State.RatesLoadingState)
        fetchRatesUseCase.invoke(UseCase.None()).also {

            it.either(::handleFailure) { result ->

                _states.apply {
                    viewModelScope.launch(Dispatchers.Main) {
                        addSource(result.asLiveData()) { list ->
                            requestFetchRates.postValue(list)
                        }
                    }
                }
            }
        }
    }
}