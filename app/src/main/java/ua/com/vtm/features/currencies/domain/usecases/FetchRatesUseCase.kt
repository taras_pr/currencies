package ua.com.vtm.features.currencies.domain.usecases

import ua.com.vtm.core.common.UseCase
import ua.com.vtm.features.currencies.data.local.rates.RateDto
import ua.com.vtm.features.currencies.domain.RatesRepository
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * @author vtm on 2021-04-18.
 */

@Reusable
class FetchRatesUseCase @Inject constructor(
    private val repository: RatesRepository
): UseCase<Flow<List<RateDto?>>, UseCase.None>() {

    override suspend fun run(params: None) = repository.fetchRates()
}