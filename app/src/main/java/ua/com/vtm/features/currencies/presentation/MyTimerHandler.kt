package ua.com.vtm.features.currencies.presentation

import java.util.*
import javax.inject.Inject

/**
 * @author vtm on 2021-04-19.
 */
class MyTimerHandler @Inject constructor() {

    private var timer: Timer? = null

    fun cancelTimer() {
        timer?.cancel()
    }

    fun startTimer(block: (Unit) -> Unit) {

        timer = Timer()
        timer?.schedule(object : TimerTask() {
            override fun run() {
                block.invoke(Unit)
            }
        }, 1000, 1000)
    }
}