package ua.com.vtm.features.currencies.domain.usecases

import ua.com.vtm.core.common.UseCase
import ua.com.vtm.features.currencies.domain.RatesRepository
import ua.com.vtm.features.currencies.presentation.RatePresentation
import dagger.Reusable
import javax.inject.Inject

/**
 * @author vtm on 2021-04-18.
 */

@Reusable
class ChangeRateUseCase @Inject constructor(
    private val repository: RatesRepository
): UseCase<Unit, RatePresentation>() {

    override suspend fun run(params: RatePresentation) = repository.changeRate(params)
}