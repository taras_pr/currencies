package ua.com.vtm.features.currencies.data.local.rates

import androidx.room.Entity
import androidx.room.PrimaryKey
import ua.com.vtm.features.currencies.presentation.RatePresentation

/**
 * @author vtm on 2021-04-17.
 */
@Entity
data class RateDto(
    @PrimaryKey var symbol: String = "",
    val name: Int = -1,
    var value: Double = 0.0,
    val image: Int = -1,
    var isDefault: Boolean = false
) {

    fun mapperToRatePresentation() = RatePresentation(
        symbol = this.symbol,
        name = this.name,
        value = this.value,
        image = this.image,
        isDefault = this.isDefault
    )
}