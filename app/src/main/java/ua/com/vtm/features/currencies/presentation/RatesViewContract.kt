package ua.com.vtm.features.currencies.presentation

import androidx.lifecycle.LiveData

/**
 * @author vtm on 2021-04-18.
 */
sealed class RatesViewContract {

    interface ViewModel {

        val states: LiveData<ViewState.State>
        val events: LiveData<ViewState.Event>

        fun invokeAction(actionToInvoke: Action)
    }

    sealed class Action: RatesViewContract() {

        object InitPageAction : Action()
        object RefreshRatesAction : Action()
        data class ChangeRateAction(val rate: RatePresentation) : Action()
    }

    sealed class ViewState: RatesViewContract() {

        sealed class State: ViewState() {

            data class RatesSuccessState(val response: RatesPresentationResponse): State()
            object RatesLoadingState: State()
        }

        sealed class Event: ViewState() {}
    }
}