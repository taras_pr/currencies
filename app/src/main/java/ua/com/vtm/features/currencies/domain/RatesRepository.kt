package ua.com.vtm.features.currencies.domain

import ua.com.vtm.core.common.EitherResult
import ua.com.vtm.features.currencies.data.local.rates.RateDto
import ua.com.vtm.features.currencies.presentation.RatePresentation
import kotlinx.coroutines.flow.Flow

/**
 * @author vtm on 2021-04-18.
 */
interface RatesRepository {

    suspend fun refreshRates(): EitherResult<Unit>
    suspend fun changeRate(ratePresentation: RatePresentation): EitherResult<Unit>
    fun fetchRates(): EitherResult<Flow<List<RateDto?>>>
}