package ua.com.vtm.features.currencies.data.local.basecurrency

import ua.com.vtm.core.common.EitherResult

/**
 * @author vtm on 2021-04-18.
 */
interface BaseCurrencyLocalDataSource {

    suspend fun fetchBaseCurrency(): EitherResult<BaseCurrencyDto?>
    suspend fun insertBaseCurrency(baseCurrency: BaseCurrencyDto?): EitherResult<Unit>
}