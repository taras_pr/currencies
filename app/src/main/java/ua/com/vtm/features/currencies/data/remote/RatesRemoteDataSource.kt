package ua.com.vtm.features.currencies.data.remote

import ua.com.vtm.core.common.EitherResult

/**
 * @author vtm on 2021-04-17.
 */
interface RatesRemoteDataSource {

    suspend fun fetchRates(): EitherResult<RatesRemoteResponse>
}