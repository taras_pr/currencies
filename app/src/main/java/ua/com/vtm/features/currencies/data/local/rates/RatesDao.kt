package ua.com.vtm.features.currencies.data.local.rates

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

/**
 * @author vtm on 2021-04-17.
 */

@Dao
interface RatesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRates(ratesDto: List<RateDto?>?)

    @Query("SELECT * FROM rateDto ORDER BY isDefault DESC ")
    fun fetchRates(): List<RateDto?>

    @Query("SELECT * FROM rateDto ORDER BY isDefault DESC ")
    fun fetchRatesAsFlow(): Flow<List<RateDto?>>

    @Query("DELETE FROM rateDto")
    suspend fun clearRates()
}