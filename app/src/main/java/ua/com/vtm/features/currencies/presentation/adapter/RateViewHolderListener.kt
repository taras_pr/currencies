package ua.com.vtm.features.currencies.presentation.adapter

import ua.com.vtm.features.currencies.presentation.RatePresentation

/**
 * @author vtm on 2021-04-19.
 */
interface RateViewHolderListener {

    fun onChangeRateValue(rate: RatePresentation?)
}