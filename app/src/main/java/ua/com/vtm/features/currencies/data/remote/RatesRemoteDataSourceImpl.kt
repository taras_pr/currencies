package ua.com.vtm.features.currencies.data.remote

import ua.com.vtm.core.common.BaseRequester
import ua.com.vtm.core.common.EitherResult
import ua.com.vtm.core.common.MyConstants
import ua.com.vtm.core.datasource.remote.NetworkHandler
import ua.com.vtm.core.datasource.remote.RemoteApi
import javax.inject.Inject

/**
 * @author vtm on 2021-04-17.
 */

class RatesRemoteDataSourceImpl @Inject constructor(
    private val remoteApi: RemoteApi,
    networkHandler: NetworkHandler
) : BaseRequester(networkHandler), RatesRemoteDataSource {

    override suspend fun fetchRates(): EitherResult<RatesRemoteResponse> {
        return request(
            apiCall = { remoteApi.fetchRates( MyConstants.EUR) },
            transform = { it },
            default = RatesRemoteResponse.empty()
        )
    }
}