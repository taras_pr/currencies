package ua.com.vtm.features.currencies.data.local.basecurrency

import androidx.room.Entity
import androidx.room.PrimaryKey
import ua.com.vtm.core.common.MyConstants

/**
 * @author vtm on 2021-04-18.
 */

@Entity
data class BaseCurrencyDto(

    @PrimaryKey val id: Int = -1,
    var currencySymbol: String? = MyConstants.EUR,
    var euroValue: Double = 1.0
) {

    companion object {

        fun firstTime() = BaseCurrencyDto(
            currencySymbol = MyConstants.EUR,
            euroValue = 1.0
        )
    }
}