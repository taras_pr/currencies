package ua.com.vtm.features.currencies.data.local.rates

import ua.com.vtm.core.common.EitherResult
import kotlinx.coroutines.flow.Flow

/**
 * @author vtm on 2021-04-17.
 */
interface RatesLocalDataSource {

    suspend fun insertAllRates(listRatesDto: List<RateDto?>?): EitherResult<Unit>
    fun fetchAllRatesAsFlow(): EitherResult<Flow<List<RateDto?>>>
    suspend fun fetchAllRates(): EitherResult<List<RateDto?>>
    suspend fun clearAllRates(): EitherResult<Unit>
}