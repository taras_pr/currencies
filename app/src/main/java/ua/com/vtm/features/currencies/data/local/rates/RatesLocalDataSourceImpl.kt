package ua.com.vtm.features.currencies.data.local.rates

import ua.com.vtm.core.common.EitherResult
import ua.com.vtm.core.common.catching
import ua.com.vtm.core.datasource.local.AppDatabase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * @author vtm on 2021-04-17.
 */
class RatesLocalDataSourceImpl @Inject constructor(
    private val appDatabase: AppDatabase
): RatesLocalDataSource {

    override suspend fun insertAllRates(listRatesDto: List<RateDto?>?): EitherResult<Unit> {
        return catching { appDatabase.ratesDao().insertRates(listRatesDto) }
    }

    override suspend fun fetchAllRates(): EitherResult<List<RateDto?>> {
        return catching { appDatabase.ratesDao().fetchRates() }
    }

    override fun fetchAllRatesAsFlow(): EitherResult<Flow<List<RateDto?>>> {
        return catching { appDatabase.ratesDao().fetchRatesAsFlow() }
    }

    override suspend fun clearAllRates(): EitherResult<Unit> {
        return catching { appDatabase.ratesDao().clearRates() }
    }
}