package ua.com.vtm.features.currencies.data.local.basecurrency

import ua.com.vtm.core.common.EitherResult
import ua.com.vtm.core.common.catching
import ua.com.vtm.core.datasource.local.AppDatabase

/**
 * @author vtm on 2021-04-18.
 */
class BaseCurrencyLocalDataSourceImpl(
    private val appDatabase: AppDatabase
): BaseCurrencyLocalDataSource {

    override suspend fun fetchBaseCurrency(): EitherResult<BaseCurrencyDto?> {
        return catching { appDatabase.baseCurrencyDao().fetchBaseCurrency()  }
    }

    override suspend fun insertBaseCurrency(baseCurrency: BaseCurrencyDto?): EitherResult<Unit> {
        return catching { appDatabase.baseCurrencyDao().insertBaseCurrency(baseCurrency) }
    }
}