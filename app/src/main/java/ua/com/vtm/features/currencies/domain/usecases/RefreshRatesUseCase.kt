package ua.com.vtm.features.currencies.domain.usecases

import ua.com.vtm.core.common.UseCase
import ua.com.vtm.features.currencies.domain.RatesRepository
import dagger.Reusable
import javax.inject.Inject

/**
 * @author vtm on 2021-04-18.
 */

@Reusable
class RefreshRatesUseCase @Inject constructor(
    private val repository: RatesRepository
): UseCase<Unit, UseCase.None>() {

    override suspend fun run(params: None) = repository.refreshRates()
}